# react12-build-an-instagram-clone-my-code

My learning flow for course ["Build an Instagram clone"](https://react12.io/instagram-clone/) from react12.io 

Course Flow:
[x] 1 - React Core Concepts 
[x] 2 - React Hooks Deep Dive
[ ] 3 - Building our Social Media App
[ ] 4 - Advanced React Hooks (useContext, useReducer) 
[ ] 1 - GraphQL Core Concepts (with Hasura)
[ ] 2 - Building a CRUD App with Apollo React Hooks 
[ ] 3 - Material UI Crash Course 
[ ] 4 - Creating our App UI 
[ ] 1 - Integrating GraphQL with Subscriptions 
[ ] 2 - Managing State with React and Apollo 
[ ] 3 - Playing Songs / Finishing the App 
[ ] 1 - Breaking Down the Instagram UI 
[ ] 2 - Building Accounts Pages 
[ ] 3 - Making Feed Page 
[ ] 4 - Improving Navbar 
[ ] 5 - Explore and Post Pages 
[ ] 6 - Loading Skeletons and Profile Page 
[ ] 7 - Edit Profile Page and Deployment 
[ ] 1 - Setup Auth, Create Users.mp4
[ ] 2 - Signup Form Validation and Error Handling.mp4
[ ] 3 - Email Login and Third Party Auth.mp4
[ ] 4 - Me Subscription and Editing User Data.mp4
[ ] 5 - Uploading Avatars and Adding User Search.mp4
[ ] 6 - Adding New Posts and Uploading Media.mp4
[ ] 7 - Liking, Saving, and Commenting on Posts.mp4
[ ] 8 - Creating and Displaying Notifications.mp4
[ ] 9 - Profile Page, plus Following and Unfollowing Users.mp4
[ ] 10 - Finishing Explore Page.mp4
[ ] 11 - Add User Feed with Infinite Scroll.mp4
[ ] 12 - Finishing App.mp4